package com.example.u2_practicas.practica2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.u2_practicas.R
import kotlinx.android.synthetic.main.activity_practica2_activity.*

const val DURATION_RESULT = 3000

class practica2_activity : AppCompatActivity() {
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == RESULT_OK){
            when(requestCode){
                DURATION_RESULT->{
                    if(data!= null){
                        val minutes = data.getIntExtra("Duration",1)
                        if(minutes >= 60){
                            p2tvSelected.text = resources.getQuantityString(R.plurals.pluralsHoras, minutes/60,minutes/60)
                        }else{
                            p2tvSelected.text = "$minutes Minutes"
                        }
                    }
                }

            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica2_activity)
        btnP2_SelectDuration.setOnClickListener{
            val intent = Intent(this, DurationSelector::class.java)
            startActivityForResult(intent, DURATION_RESULT)

        }

    }
}