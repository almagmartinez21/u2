package com.example.u2_practicas.practica1

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.telephony.SmsManager
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.example.u2_practicas.R
import kotlinx.android.synthetic.main.activity_practica1_activity.*

class practica1_activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica1_activity)
        var statusRadio = "INTENT"
        p1rg.check(p1rbINTENT.id)

        btnp1call.setOnClickListener{
            val statusSwitch = p1swOpcion.isChecked
            val numberPhone = p1txtphone.text.toString()

            if(!statusSwitch) {
                if (validateCallPermissions()) { //Tenemos permiso PARA LLAMAR
                    val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$numberPhone"))
                    startActivity(intent)

                } else {//
                    val permission = arrayOf(android.Manifest.permission.CALL_PHONE)
                    ActivityCompat.requestPermissions(this, permission, 101)

                }
            }else{
                val bodyMessage = p1txtMessage.text.toString()
                if (validateSMSPermissions()) {
                    if(statusRadio == "INTENT") {
                        //Tenemos permiso PARA MANDAR SMS
                        //INTENT PARA ENVIAR MENSAJES A TRAVES DE UNA APLICACION "DEFAULT" DE MENSAJES
                        val intent = Intent(Intent.ACTION_SENDTO, Uri.parse("smsto:$numberPhone"))
                        intent.putExtra("sms_body",bodyMessage)
                        startActivity(intent)
                    }else{
                        val smsManager = SmsManager.getDefault() as SmsManager
                        smsManager.sendTextMessage("$numberPhone", null, "$bodyMessage", null, null)
                        Toast.makeText(this, "Mensaje Enviado Correctamente", Toast.LENGTH_SHORT).show()
                    }
                } else {//
                    val permission = arrayOf(android.Manifest.permission.SEND_SMS)
                    ActivityCompat.requestPermissions(this, permission, 102)

                }

            }
        }
        p1swOpcion.setOnCheckedChangeListener { butttonView, isChecked ->
            if(isChecked){
                p1swOpcion.text = "Mensaje"
                btnp1call.text = "Enviar Mensaje"
                p1txtMessage.visibility = View.VISIBLE
                p1tvMensaje.visibility  = View.VISIBLE
                p1rg.visibility = View.VISIBLE
            }else{
                p1swOpcion.text = "Llamada"
                btnp1call.text = "Llamar"
                p1txtMessage.visibility = View.GONE
                p1tvMensaje.visibility  = View.GONE
                p1rg.visibility = View.GONE


            }
        }
        p1rg.setOnCheckedChangeListener{roup, checkedId ->
            when(p1rg.findViewById<RadioButton>(checkedId)) {
                p1rbAPI->{
                    statusRadio = "API"

                }
                p1rbINTENT->{
                    statusRadio = "INTENT"

                }

            }
        }
    }

    private fun validateCallPermissions(): Boolean{
        val permissions = arrayOf(android.Manifest.permission.CALL_PHONE)
        return ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
    }
    private fun validateSMSPermissions(): Boolean{
        val permissions = arrayOf(android.Manifest.permission.SEND_SMS)
        return ActivityCompat.checkSelfPermission(this, android.Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED
    }
}