package com.example.u2_practicas.practica3

enum class Genero (var gender: String) {
    HOMBRE("Hombre"),
    MUJER("Mujer"),
    NO_BINARIO("No Binario"),
    NO_IDENTIFICADO("No Identificado")
}