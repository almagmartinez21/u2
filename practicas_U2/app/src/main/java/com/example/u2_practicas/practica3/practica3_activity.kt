package com.example.u2_practicas.practica3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.u2_practicas.R
import kotlinx.android.synthetic.main.activity_practica3_activity.*

class practica3_activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        var genero: String = Genero.HOMBRE.gender
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica3_activity)

        p3btnEnviarDatos.setOnClickListener{
            val bundle = Bundle()
            val intent = Intent(this, practica3_bundle::class.java)
            if(p3txtEdad.text.isNotEmpty() && p3txtApellido.text.isNotEmpty() &&
                    p3txtEdad.text.isNotEmpty() && p3txtSalario.text.isNotEmpty()) {
                bundle.putString("Nombre", p3txtNombre.text.toString())
                bundle.putString("Apellido", p3txtApellido.text.toString())
                bundle.putInt("Edad", p3txtEdad.text.toString().toInt())
                bundle.putInt("Salario", p3txtSalario.text.toString().toInt())
                bundle.putString("Genero", genero)
                intent.putExtras(bundle)
                startActivity(intent)
            }else{
                Toast.makeText(this, "Esta dejando campos sin rellenar", Toast.LENGTH_SHORT).show()
            }


        }
        p3rgGenero.setOnCheckedChangeListener{group,checkedId->
            when(checkedId) {
                R.id.p3rbHombre ->{
                    genero = Genero.HOMBRE.gender
                }
                R.id.p3rbMujer->{
                    genero = Genero.MUJER.gender
                }
                R.id.p3rbNoBinario->{
                    genero = Genero.NO_BINARIO.gender
                }
                R.id.p3rbNoIdentificado->{
                    genero = Genero.NO_IDENTIFICADO.gender
                }
            }
        }
    }
}