package com.example.u2_practicas.practica2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.u2_practicas.R
import kotlinx.android.synthetic.main.time_list.view.*

class DurationAdapter(private val listener: (Int) -> Unit): RecyclerView.Adapter<DurationAdapterViewHolder>() {
    private var list = mutableListOf<Int>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DurationAdapterViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.time_list,parent,false)
        return DurationAdapterViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: DurationAdapterViewHolder, position: Int) {
        holder.setData(list[position], listener)

    }

    override fun getItemCount(): Int {
        return list.size
    }
    fun setList(list: List<Int>){
        this.list.addAll(list)
        notifyDataSetChanged()
    }

}

class DurationAdapterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
    fun setData(duration: Int, listener: (Int) -> Unit){
        itemView.apply {
            var horas = duration/60
            if(horas > 0){
                p2tvMinutes.text = context.resources.getQuantityString(R.plurals.pluralsHoras,duration/60,duration/60)
            }else{
                p2tvMinutes.text = "$duration Minutos"
            }
            setOnClickListener{ listener.invoke(duration)}

        }
    }

}