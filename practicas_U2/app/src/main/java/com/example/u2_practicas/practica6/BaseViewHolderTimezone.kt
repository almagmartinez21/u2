package com.example.u2_practicas.practica6

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolderTimezone<T>(itemView: View): RecyclerView.ViewHolder(itemView){
    abstract fun bind(item: T ,position: Int)
}
