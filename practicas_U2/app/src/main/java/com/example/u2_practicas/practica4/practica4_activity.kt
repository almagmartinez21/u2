package com.example.u2_practicas.practica4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.u2_practicas.R
import kotlinx.android.synthetic.main.activity_practica4_activity.*

class practica4_activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica4_activity)
        p4btnAlertaOpciones.setOnClickListener{
            val builder = AlertDialog.Builder(this)
            builder.setMessage("Este es un cuadro de dialogo en Android con varias opciones de respuesta ♥")
                    .setTitle("Alert Dialog")
                    .setPositiveButton("Sí"){dialog,which->
                        Toast.makeText(this, "Dijiste que sí", Toast.LENGTH_SHORT).show()
                    }
                    .setNegativeButton("No"){dialog,which->
                        Toast.makeText(this, "Dijiste que no", Toast.LENGTH_SHORT).show()
                    }
                    .setNeutralButton("Me vale"){dialog, which->
                        Toast.makeText(this, "Te valió el cuadro", Toast.LENGTH_SHORT).show()
                    }
            val dialog = builder.create()
            dialog.show()
        }
        p4btnListaSencilla.setOnClickListener{
            val colort = arrayOf("Rojo","Azul","Verde","Amarillo","Narajna")
            val builder = AlertDialog.Builder(this)
                    .setTitle("Alert Dialog Radio Button")
                    .setItems(colort){dialog, which->
                        Toast.makeText(this, "Seleccionaste: ${colort[which]}", Toast.LENGTH_SHORT).show()
                    }
            val dialog = builder.create()
            dialog.show()
        }
        p4btnRadioButton.setOnClickListener{
            val colort = arrayOf("Rojo","Azul","Verde","Amarillo","Narajna")
            val builder = AlertDialog.Builder(this)
                    .setTitle("Alert Dialog Radio Button")
                    .setSingleChoiceItems(colort,0){dialog, which->
                        Toast.makeText(this, "Seleccionaste: ${colort[which]}", Toast.LENGTH_SHORT).show()
                    }
            val dialog = builder.create()
            dialog.show()
        }
        val selectedItems = arrayListOf<Int>()
        p4btnCheckBox.setOnClickListener{
            val colort = arrayOf("Rojo","Azul","Verde","Amarillo","Narajna")
            val builder = AlertDialog.Builder(this)
                    .setTitle("Alert Dialog Check Box")
                    .setMultiChoiceItems(colort,null){dialog, which, isChecked->
                        if(isChecked){
                            selectedItems.add(which)
                            Toast.makeText(this, "Seleccionaste: ${selectedItems.size} colores", Toast.LENGTH_SHORT).show()
                        }else if(selectedItems.contains(which))
                            selectedItems.remove(which)
                    }
            val dialog = builder.create()
            dialog.show()
        }
        p4btnCustom.setOnClickListener{
            Common.customDialog(this){yes ->
                if(yes){
                    Toast.makeText(this, "Yes", Toast.LENGTH_SHORT).show()
                }else{
                    Toast.makeText(this, "NO", Toast.LENGTH_SHORT).show()
                }
            }.show()
        }
    }
}