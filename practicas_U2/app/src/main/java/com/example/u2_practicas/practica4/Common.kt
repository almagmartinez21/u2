package com.example.u2_practicas.practica4

import android.app.Activity
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import com.example.u2_practicas.R
import kotlinx.android.synthetic.main.custom_dialog.view.*

object Common {
    fun customDialog(activity: Activity,listener: (Boolean)->Unit): AlertDialog{
        val builder = AlertDialog.Builder(activity)
        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.custom_dialog,null)
        builder.setView(view)
        val dialog = builder.create()

        dialog.window?.attributes?.windowAnimations = R.style.Widget_AppCompat_PopupWindow
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val wmlmp: WindowManager.LayoutParams? = dialog.window?.attributes
        wmlmp?.gravity = Gravity.BOTTOM
        view.p4btnCustomSI.setOnClickListener{
            listener.invoke(true)
            dialog.dismiss()
        }
        view.p4btnCustomNO.setOnClickListener{
            listener.invoke(false)
            dialog.dismiss()
        }
        return dialog

    }

}