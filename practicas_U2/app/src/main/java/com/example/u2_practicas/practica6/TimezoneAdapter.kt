package com.example.u2_practicas.practica6

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.u2_practicas.R
import kotlinx.android.synthetic.main.timezone_list.view.*


class TimezoneAdapter(
    private val context: Context,
    private val listaTimezone:List<Citiesandtimezones.CitiesTimezones>,
    private val itemClickListener: OnCityClickListener,
): RecyclerView.Adapter<BaseViewHolderTimezone<*>>() {
    interface OnCityClickListener{
        fun onItemClick(item: Citiesandtimezones.CitiesTimezones)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolderTimezone<*> {
        return ViewHolderTimezone(LayoutInflater.from(context).inflate(R.layout.timezone_list,parent,false))
    }

    override fun onBindViewHolder(holderTimezone: BaseViewHolderTimezone<*>, position: Int) {
        when (holderTimezone){
            is ViewHolderTimezone -> holderTimezone.bind(listaTimezone[position],position)
            else -> throw IllegalArgumentException("No hay view model")
        }
    }

    override fun getItemCount(): Int = listaTimezone.size

    inner class ViewHolderTimezone(itemView: View):BaseViewHolderTimezone<Citiesandtimezones.CitiesTimezones>(itemView){
        override fun bind(item: Citiesandtimezones.CitiesTimezones, position: Int){
            itemView.setOnClickListener { itemClickListener.onItemClick(item) }
            itemView.p6tvCiudad.text = item.country
            itemView.p6tvTimezone.text = item.timezone



        }
    }
}

