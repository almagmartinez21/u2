package com.example.u2_practicas.practica3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.u2_practicas.R
import kotlinx.android.synthetic.main.activity_practica3_bundle.*

class practica3_bundle : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica3_bundle)
        var bundle = Bundle()
        bundle = intent.extras ?: Bundle()
        val nombre = bundle.getString("Nombre")
        val apellido = bundle.getString("Apellido")
        val edad = bundle.getInt("Edad")
        val salario = bundle.getInt("Salario")
        val genero = bundle.getString("Genero")
        p3tvBundleResult.text = String.format(getString(R.string.info),nombre,apellido,edad,salario,genero)

    }
}