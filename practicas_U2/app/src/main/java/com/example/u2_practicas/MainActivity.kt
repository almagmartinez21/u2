package com.example.u2_practicas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.u2_practicas.practica1.practica1_activity
import com.example.u2_practicas.practica2.practica2_activity
import com.example.u2_practicas.practica3.practica3_activity
import com.example.u2_practicas.practica4.practica4_activity
import com.example.u2_practicas.practica5.practica5_activity
import com.example.u2_practicas.practica6.practica6_activity
import kotlinx.android.synthetic.main.activity_main.*

class  MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnPractica1.setOnClickListener{
            val intent = Intent(this, practica1_activity::class.java)
            startActivity(intent)
        }
        btnPractica2.setOnClickListener{
            val intent = Intent(this, practica2_activity::class.java)
            startActivity(intent)
        }
        btnPractica3.setOnClickListener{
            val intent = Intent(this, practica3_activity::class.java)
            startActivity(intent)
        }

        btnPractica4.setOnClickListener{
            val intent = Intent(this, practica4_activity::class.java)
            startActivity(intent)
        }
        btnPractica5.setOnClickListener{
            val intent = Intent(this,practica5_activity::class.java)
            startActivity(intent)
        }
        btnPractica6.setOnClickListener{
            val intent = Intent(this, practica6_activity::class.java)
            startActivity(intent)
        }
    }
}