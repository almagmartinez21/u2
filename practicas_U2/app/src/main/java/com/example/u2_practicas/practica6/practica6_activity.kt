package com.example.u2_practicas.practica6

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.u2_practicas.R
import kotlinx.android.synthetic.main.activity_practica6_activity.*
import org.json.JSONArray
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.StringWriter

class practica6_activity : AppCompatActivity(),TimezoneAdapter.OnCityClickListener{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica6_activity)

        val inputS = resources.openRawResource(R.raw.cities_and_timezones)
        val writer = StringWriter()
        val buffer = CharArray(1024)
        inputS.use { input ->
            val reader = BufferedReader(InputStreamReader(input,"UTF-8"))
            var n: Int
            while (reader.read(buffer).also { n = it } != -1 ){
                writer.write(buffer,0,n)
            }
        }
        val jsonArray = JSONArray(writer.toString())
        val listTimeZone  = Citiesandtimezones(jsonArray).cities
        setupRecyclerView(listTimeZone)
    }
    private fun setupRecyclerView(listTimeZone: ArrayList<Citiesandtimezones.CitiesTimezones>){
        p6rvctz.layoutManager = LinearLayoutManager(this)
        p6rvctz.adapter = TimezoneAdapter(this, listTimeZone, this)

    }
    override fun onItemClick(item: Citiesandtimezones.CitiesTimezones) {
        Toast.makeText(this, "Nombre: ${item.name} con zona horaria: ${item.timezone}", Toast.LENGTH_SHORT).show()

    }
}