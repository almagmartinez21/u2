package com.example.u2_practicas.practica6

import org.json.JSONArray
import org.json.JSONObject

class Citiesandtimezones {
    var cities = arrayListOf<CitiesTimezones>()
    constructor(jsonArray: JSONArray){
        for (i in 0 until jsonArray.length()){
            val data = jsonArray.getJSONObject(i)
            cities.add(CitiesTimezones(data))
        }

    }
    class CitiesTimezones(jsonObject: JSONObject){
        var name: String = ""
        var country: String = ""
        var timezone: String = ""
        init{
            name = jsonObject.getString("name")
            country = jsonObject.getString(("country"))
            timezone = jsonObject.getString("timeZoneName")
        }
    }
}