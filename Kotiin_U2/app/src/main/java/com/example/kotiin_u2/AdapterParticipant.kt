package com.example.kotiin_u

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast.LENGTH_SHORT
import androidx.recyclerview.widget.RecyclerView
import com.example.kotiin_u2.Participant
import com.example.kotiin_u2.R
import android.widget.Toast as Toast1

class Adapterparticipant : RecyclerView.Adapter<Adapterparticipant.ViewHolder>() {

    var infor: MutableList<Participant> = ArrayList() //Lista de objeto participante
    lateinit var contexto: Context//sirve para definir despues algo que aun no esta disponible

    fun Adapterparticipant(lista: MutableList<Participant>, contexto: Context) {
        this.infor = lista
        this.contexto = contexto
    }

    override fun onBindViewHolder(//Enlazar con los datos
        holder: Adapterparticipant.ViewHolder,
        position: Int
    ) {
        val inf = infor[position]
        holder.bind(inf, contexto)
    }

    override fun getItemCount(): Int { //Cantidad de elementos
        return infor.size
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): Adapterparticipant.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)//carga un inflador
        return ViewHolder( //devuelve una lista
            layoutInflater.inflate(
                R.layout.datos,
                parent,
                false
            )
        )

    }

    class ViewHolder(view:View): RecyclerView.ViewHolder(view) {
        private val nombre= view.findViewById(R.id.tvnombre) as TextView
        private val apellido= view.findViewById(R.id.tvapellido) as TextView
        private val edad= view.findViewById(R.id.tvedad) as IntView
        private val profesion= view.findViewById(R.id.tvprofesion) as TextView

        fun bind(participant: Participant, context: Context){
            nombre.text=participant.nombre
            apellido.text=participant.apellido
            edad.=participant.edad
            profesion.text=participant.profesion
            itemView.setOnClickListener {
                Toast1.makeText(
                    context,
                    participant.nombre,
                    android.widget.Toast.LENGTH_SHORT
                ).show()

            }
            }
        }

    class IntView {

    }
}

}