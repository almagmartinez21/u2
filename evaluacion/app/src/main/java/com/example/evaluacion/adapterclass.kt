package com.example.evaluacion

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.practicas_uni2.R

class Adapterclass: RecyclerView.Adapter<AdapterclassViewHolder>() {

    val list = mutableListOf<Clima>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterclassViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_lista7,parent,false)
        return AdapterclassViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: AdapterclassViewHolder, position: Int) {
        holder.setData(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }
}

class AdapterclassViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
    fun setData(clima: Clima){

    }

}


data class Clima (var ciudad:String, var temperatura:Int)