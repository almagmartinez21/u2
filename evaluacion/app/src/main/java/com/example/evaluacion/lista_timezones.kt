package com.example.evaluacion
import org.json.JSONArray
import org.json.JSONObject

class lista_timezones {
    var cities = arrayListOf<CitiesTimezones>()

    constructor()

    constructor(jsonArray: JSONArray){
        for (i in 0 until  jsonArray.length()){
            val data = jsonArray.getJSONObject(i)
            cities.add(CitiesTimezones(data))
        }

    }
    class CitiesTimezones (jsonObject: JSONObject){
        var country: String =""
        var temperature: String =""
        var timezones: String=""

        init{
            country = jsonObject.getString("Nombre")
            temperature = jsonObject.getString("Ciudad")
            timezones = jsonObject.getString("TimeZoneName")
        }

    }
}